
$(document).ready(function () {
    $('#link').click(function(){
        $("html, body").animate({
            scrollTop:0}, 600);
    });
});
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("link").style.display = "block";
    } else {
        document.getElementById("link").style.display = "none";
    }
}